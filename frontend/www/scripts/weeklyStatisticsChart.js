var weeknr = 0;
var weeklyStatisticsData = [];
var currentAthelte = null;

function initiateChartValues(btnNext, btnprev, chartContainer){
    window.btnNext = btnNext;
    window.btnPrev = btnprev;
    window.chartContainer = chartContainer;
}

// Renders the highcharts graph
function renderWeeklyStatistics() {

    if(window.weeklyStatisticsData.length == 0){
        document.getElementById(chartContainer).innerHTML =`<h3>No workouts registered</h3>`
    }else{
        Highcharts.chart(chartContainer, {
            title: {
                text: 'Weekly Statistics'
            },
            yAxis: {
                title: {
                    text: 'Number of Workouts (Cummulative)'
                },
                allowDecimals: false,
            },
            xAxis: {
                type:'datetime'
            },
            legend:{ enabled:false },
            series: [{
                name: 'Workouts',
                data: window.weeklyStatisticsData[window.weeknr]
            }]
        });
    }
}

// Fetches weekly statistics data for the logged in user
/* Data format: [
    [ (current week)
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value]
    ],
    [ (last week)
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value],
        [date, value]
    ]
] */
async function retrieveWeeklyStatistics(user){
    const response = await sendRequest("GET", `${HOST}/api/workout-statistics/?user=${user}`);
    const data = await response.json();
    window.weeklyStatisticsData = parseStatisticsData(data.results);
}

// Changes which week the weekly statistics graph shows
function changeWeekNr(weekChange){
    const {btnPrevElem, btnNextElem} = getBtnElements();
    const newWeekNr = window.weeknr + weekChange;
    const totalWeeks = window.weeklyStatisticsData.length;

    if(newWeekNr + 1 >= totalWeeks){
        // Last week
        btnPrevElem.disabled = true;
    } else if( newWeekNr <= 0){
        // Current week
        btnNextElem.disabled = true;
    }
    if( btnPrevElem.disabled === true && newWeekNr + 1 < totalWeeks){
        // Moving from last to second-to-last week 
        btnPrevElem.disabled = false;
    } 
    if( btnNextElem.disabled === true && newWeekNr > 0){
        // Moving from first to second week
        btnNextElem.disabled = false;
    }
    window.weeknr += weekChange;
    renderWeeklyStatistics();
}

// Initial load of the statistics graph for the given user
async function chartFirstLoad(user){
    await retrieveWeeklyStatistics(user);

    const {btnPrevElem, btnNextElem} = getBtnElements();

    btnNextElem.disabled = true;
    if(1 >= window.weeklyStatisticsData.length){
        // One or zero weeks in total
        btnPrevElem.disabled = true;
    }else{
        btnPrevElem.disabled = false;
    }
    window.weeknr = 0;

    renderWeeklyStatistics();
}

function getBtnElements(){
    const btnPrevElem = document.getElementById(btnPrev);
    const btnNextElem = document.getElementById(btnNext);
    return {btnPrevElem, btnNextElem}
}
