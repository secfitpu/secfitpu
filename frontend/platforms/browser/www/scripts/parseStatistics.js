function parseStatisticsData(data) {
    
    // check if no workouts registered
    if(data.length == 0){
        return [];
    }

    let workoutDates = data.map(obj => new Date(obj.date));
    workoutDates = workoutDates.map(date => new Date(Date.UTC(date.getFullYear(),date.getMonth(), date.getDate()))) // deserialiing 
    
    // get first and last day in weeks
    const sundayInLastWorkoutWeek = workoutDates[0].is().sunday() ? workoutDates[0] : new Date(workoutDates[0].getTime()).next().sunday();
    const firstDate = workoutDates[workoutDates.length-1];
    const mondayInFirstWorkoutWeek = firstDate.is().monday() ? firstDate : new Date(firstDate.getTime()).last().monday();
    
    // add all days from sundayInLastWorkoutWeek to mondayFirstWorkoutWeek to result array
    const tempDay = new Date(sundayInLastWorkoutWeek.getTime()).addDays(1);
    let result = []
    while (!tempDay.equals(mondayInFirstWorkoutWeek)) {
        tempDay.addDays(-1);
        result.push([new Date(tempDay.getTime()), 0]);
    }
    
    // count workouts
    for (let i = 0; i < workoutDates.length; i++) {
        for (let j = 0; j < result.length; j++) {
            if (workoutDates[i].equals(result[j][0])) {
                result[j][1] += 1
            }
        }
    }
    result = toMatrix(result, 7); // divide array into weeks

    // make workout count cumulative per week
    for (let i = 0; i < result.length; i++) {
        let sum = 0;
        for (let j = 0; j < result[0].length; j++) {
            sum += result[i][j][1]
            result[i][j][1] = sum
            result[i][j][0] = result[i][j][0].getTime();
        }
    }
    return result;
}

// convert array into two-dimantional array
const toMatrix = (arr, width) => 
    arr.reduce((rows, key, index) => (index % width == 0 ? rows.push([key]) 
      : rows[rows.length-1].unshift(key)) && rows, []);