// Function that runs at page load
window.addEventListener("DOMContentLoaded", async () => {
    await displayProfileInformation();
});

// Checks if user is logged in
// If logged in, add profile page html elements and render weekly statistics
async function displayProfileInformation() {
    let user = null;
    let response = await sendRequest("GET", `${HOST}/api/user-profiles/?user=current`);
    if (!response.ok) {
        // COULD NOT RETRIEVE CURRENTLY LOGGED IN USER
        document.getElementById("profile-container").innerHTML =`
            <h5 class="text-center">Log in to see your profile</h5>`;
    } else {
        let data = await response.json();
        user = data.results[0];

        const username = "username";
        const email = "email";

        initiateChartValues("btn-next", "btn-previous", "container");

        //Adds the profile attributes
        document.getElementById("profile-container").innerHTML =`
            <div class="row mt-5">
                <div class="col-auto">
                    <h5>Username:</h5>
                </div>
                <div class="col">
                    <p id=${username}></p>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-auto">
                    <h5>Email:</h5>
                </div>
                <div class="col">
                    <p id="${email}"></p>
                </div>
            </div>

            <div style="margin-top:2em">
                <button id=${btnPrev} class="btn btn-primary">Previous Week</button>
                <button disabled id=${btnNext} class="btn btn-primary">Next Week</button>
            </div>

            <figure class="highcharts-figure" style="max-width: 100em; height: 400px; ">
                <div id="container"></div>
            </figure>
            `;
        
        document.getElementById(username).innerText = user.username;
        document.getElementById(email).innerText = user.email;

        const {btnPrevElem, btnNextElem} = getBtnElements();
        btnPrevElem.onclick = function(){changeWeekNr(1)};
        btnNextElem.onclick = function(){changeWeekNr(-1)};

        chartFirstLoad(`current`);
    }

    return user;
}
