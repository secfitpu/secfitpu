describe("Offers", () => {
  const url = "http://localhost:8000/";

  const test_user_username = "test_user";
  const test_user_password = "test_password";
  const test_user_3_username = "test_user_3";

  const baseOption = {
    status: "pending",
    recipient: url + "api/users/3/",
  };

  // Base options object for HTTP request
  const baseOptions = {
    url: url + "api/offers/",
    method: null,
    auth: null,
    body: baseOption,
    failOnStatusCode: true,
  };

  before(async () => {
    // Get access token
    await loginUser(test_user_username);
  });

  // Login a test user
  const loginUser = (username) => {
    const body = { username: username, password: test_user_password };
    return new Promise((resolve, reject) => {
      cy.request("POST", url + "api/token/", body).then((resp) => {
        try {
          baseOptions.auth = { bearer: resp.body.access };
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  const sendRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  it("Send offer", async () => {
    const options = Object.assign({}, baseOptions);
    options.method = "POST";
    const response = await sendRequest(options, 201);
    expect(response.owner).to.eq(test_user_username);
    expect(response.status).to.eq(options.body.status);
    expect(response.recipient).to.eq(options.body.recipient);

    // send another request so that we have 2 in total. Want to both test decline and accept offer
    await sendRequest(options, 201);
  });

  it("View sent offer", async () => {
    const options = Object.assign({}, baseOptions);
    options.method = "GET";
    options.url = url + `api/offers/?status=${baseOption.status}&category=sent`;
    const response = await sendRequest(options, 200);
    expect(response.count).to.be.greaterThan(0);
  });

  it("View and decline recieved offer", async () => {
    await loginUser(test_user_3_username);
    const options = Object.assign({}, baseOptions);

    // Get offer
    options.method = "GET";
    options.url =
      url + `api/offers/?status=${baseOption.status}&category=received`;
    const getResponse = await sendRequest(options, 200);

    // Decline offer
    options.method = "PATCH";
    options.url = getResponse.results[0].url;
    options.body = { status: "declined" };
    const patchResponse = await sendRequest(options, 200);
    expect(patchResponse.status).to.eq(options.body.status);
  });

  it("View and accept recieved offer", async () => {
    const options = Object.assign({}, baseOptions);

    // Get offer
    options.method = "GET";
    options.url =
      url + `api/offers/?status=${baseOption.status}&category=received`;
    const getResponse = await sendRequest(options, 200);

    // Accept offer
    options.method = "PATCH";
    options.url = getResponse.results[0].url;
    options.body = { status: "accepted" };
    const patchResponse = await sendRequest(options, 200);
    expect(patchResponse.status).to.eq(options.body.status);
  });
});
