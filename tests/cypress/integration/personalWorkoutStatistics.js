describe("Profile page tests", () => {
  before(() => {
    cy.visit("/login.html"); // go to baseUrl, baseUrl defined in cypress.json
    cy.intercept("/api/*").as("getData"); // create route getData

    // Log in user
    cy.get("#form-login input").eq(0).type("test_user"); // username input
    cy.get("#form-login input").eq(1).type("test_password"); // password input
    cy.get("#btn-login").click();
    cy.wait("@getData");
  });

  it("Successfully load profile page", () => {
    cy.visit("/profile.html");
    cy.intercept("/api/").as("apiData");
    cy.wait("@apiData");
    cy.get("#username").should("have.text", "test_user");
    cy.get("#email").should("have.text", "test@mail.com");
  });

  it("Personal workout statistics loaded", () => {
    cy.get(".highcharts-figure");
    cy.get(".highcharts-point");
  });

  it("Buttons work", () => {
    cy.get("#btn-next").should('be.disabled');
    cy.get("#btn-previous").should('not.be.disabled');
    cy.get('#btn-previous').click();
    cy.get("#btn-next").should('not.be.disabled');
    cy.get("#btn-previous").should('not.be.disabled');
    cy.get('#btn-next').click();
    cy.get("#btn-next").should('be.disabled');
    cy.get("#btn-previous").should('not.be.disabled');
  });
});
