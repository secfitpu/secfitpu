describe("Registration boundary tests", () => {
  const url = "http://localhost:8000/";

  // valid values for creating user
  const baseRegister = {
    username: "name",
    email: "test@mail.com",
    password: "test_password",
    password1: "test_password",
    phone_number: 12345678,
    country: "country",
    city: "city",
    street_address: 1,
  };

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/users/",
    method: "POST",
    auth: null,
    body: baseRegister,
    failOnStatusCode: false,
    form: true,
  };

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body.id);
        } catch (error) {
          resolve();
        }
      });
    });
  };

  const randomString = (radix, length) => {
    // generate randoms string. Max length: 11
    // radix=10 => random number
    // radix=36 => all numbers and a-z lowercase letters
    return Math.random().toString(radix).substr(2, length);
  };

  it("Username boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions)); // deep copy

    options.body.username = null;
    await testRequest(options, 400);

    options.body.username = "";
    await testRequest(options, 400);

    options.body.username = randomString(36, 4); // random string of length 4
    await testRequest(options, 400, "Username should have at least 5 characters");

    options.body.username = randomString(36, 5); // random string of lenght 5 [0-9 a-z]
    await testRequest(options, 201);

    options.body.username = parseInt(randomString(10, 5)); // random number of length 5
    await testRequest(options, 201);

    options.body.username = "invalid string";
    await testRequest(options, 400);

    options.body.username = "@-+_" + randomString(36, 5); // string with valid special characters
    await testRequest(options, 201);

    options.body.username = randomString(36, 7).repeat(2) + "a"; // random string of length 15
    await testRequest(options, 201);

    options.body.username = randomString(36, 8).repeat(2); // random string of length 16
    await testRequest(options, 400, "Username should have max 15 characters");
  });

  it("Email boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.username = randomString(36, 7); // need to have unique username per test
    options.body.email = "test@mail.com";
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.email = null;
    await testRequest(options, 400, "Email should not be null");

    options.body.username = randomString(36, 7);
    options.body.email = "";
    await testRequest(options, 400, "Email should not be empty");

    options.body.username = randomString(36, 7);
    options.body.email = " @ . ";
    await testRequest(options, 400);

    options.body.username = randomString(36, 7);
    options.body.email = "a@a.c";
    await testRequest(options, 400);

    options.body.username = randomString(36, 7);
    options.body.email = "a@a$.com";
    await testRequest(options, 400);
  });

  it("Passwords boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.username = randomString(36, 7);
    options.body.password = null;
    options.body.password1 = null;
    await testRequest(options, 400);

    options.body.username = randomString(36, 7);
    options.body.password = "";
    options.body.password1 = "";
    await testRequest(options, 400);

    options.body.username = randomString(36, 7);
    options.body.password = "Pdsf1";
    options.body.password1 = "Pdsf1";
    await testRequest(options, 400, "Password should have at least 6 characters");

    options.body.username = randomString(36, 7);
    options.body.password = "Pfkss12";
    options.body.password1 = "Pfkss12";
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.password = "!#¤%&/(?`|§1";
    options.body.password1 = "!#¤%&/(?`|§1";
    await testRequest(options, 201);
  });

  it("Phone number boundry test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.username = randomString(36, 7);
    options.body.phone_number = "";
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.phone_number = null;
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.phone_number = randomString(10, 4); // 4 digit number
    await testRequest(options, 400, "Phone number min length should be 5");

    options.body.username = randomString(36, 7);
    options.body.phone_number = randomString(10, 5); // 5 digit number
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.phone_number = randomString(10, 7).repeat(2) + "1"; // 15 digit number
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.phone_number = randomString(10, 8).repeat(2); // 16 digit number
    await testRequest(options, 400, "Phone number max length should be 15"); // The E.164 standard of the International Telecommunications Union permits max length of 15 digits

    options.body.username = randomString(36, 7);
    options.body.phone_number = randomString(36, 7) + "a"; // random string with at least one letter
    await testRequest(options, 400, "Phone number should not contain letters");

    options.body.username = randomString(36, 7);
    options.body.phone_number = parseInt(randomString(10, 8)); // integer instead of string
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.phone_number = "-" + randomString(10, 8); // negative number
    await testRequest(options, 400, "Negative numbers should not be allowed");
  });

  it("Country, street adress, and city boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.username = randomString(36, 7);
    options.body.country = "";
    options.body.street_address = "";
    options.body.city = "";
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.country = null;
    options.body.street_address = null;
    options.body.city = null;
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.country = "Norway1#";
    await testRequest(options, 400, "Country should not contain numbers or special characters");

    options.body.username = randomString(36, 7);
    options.body.country = randomString(36, 10).repeat(5); // random string of length 50
    options.body.street_address = randomString(36, 10).repeat(5);
    options.body.city = randomString(36, 10).repeat(5);
    await testRequest(options, 201);

    options.body.username = randomString(36, 7);
    options.body.country = randomString(36, 10).repeat(5) + "a"; // random string of length 51
    options.body.street_address = randomString(36, 10).repeat(5) + "a";
    options.body.city = randomString(36, 10).repeat(5) + "a";
    await testRequest(options, 400);
  });
});
