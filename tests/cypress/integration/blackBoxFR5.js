describe("Black box FR5 workout visibility", () => {
  const url = "http://localhost:8000/";

  const test_user_username = "test_user";
  const test_user_password = "test_password";
  const test_user_2_username = "test_user_2";

  var privateWorkout = null;
  var coachWorkout = null;
  var publicWorkout = null;
  var coachPrivateWorkout = null;
  var coachCoachWorkout = null;

  const PU2ID = 2;
  const PR2ID = 3;
  const CO2ID = 4;
  const PR1ID = 5;
  const CO1ID = 6;

  const PU2FileID = 4;
  const PR2FileID = 5;
  const CO2FileID = 6;
  const PR1FileID = 2;
  const CO1FileID = 3;

  const fileStart = "http://localhost:8000/media/workouts/";
  const fileEnd = "/file_test.txt";

  // valid values for workout
  const baseWorkout = {
    name: "name",
    date: "2021-02-28T23:00:00.000Z",
    notes: "notes",
    visibility: null,
    exercise_instances: [
      {
        exercise: url + "api/exercises/2/",
        repetitions: "10",
        sets: "1",
      },
    ],
  };

  // Base options object for HTTP request for making a workout
  const baseOptionsPostWorkout = {
    url: url + "api/workouts/",
    method: "POST",
    auth: null,
    body: null,
    failOnStatusCode: false,
  };

  const baseOptionsGet = {
    url: null,
    method: "GET",
    auth: null,
    failOnStatusCode: false,
  };

  const baseOptionsComments = {
    url: url + "api/comments/",
    method: "POST",
    body: null,
    auth: null,
    failOnStatusCode: false,
  };

  function login(username, password, options) {
    const body = { username: username, password: password };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      options.auth = { bearer: accessToken };
    });
  }

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  async function retrieveWorkout(
    workoutId,
    username,
    password,
    statusCode = 200
  ) {
    // Retrive workout with workout id
    baseOptionsGet.url = url + "api/workouts/" + workoutId + "/";
    login(username, password, baseOptionsGet);
    return await testRequest(baseOptionsGet, statusCode);
  }

  async function retrieveWorkouts(username, password, statusCode = 200) {
    // Retrive workout with workout id
    baseOptionsGet.url = url + "api/workouts/";
    login(username, password, baseOptionsGet);
    return await testRequest(baseOptionsGet, statusCode);
  }

  function checkIfIncluded(list, elem) {
    return checkIfIdIncluded(list, elem.id);
  }

  function checkIfIdIncluded(list, id) {
    var boolean = false;
    list.forEach((listElem) => {
      if (listElem.id === id) {
        boolean = true;
        return;
      }
    });
    return boolean;
  }

  before(async () => {
    publicWorkout = await retrieveWorkout(
      PU2ID,
      test_user_2_username,
      test_user_password
    );
    privateWorkout = await retrieveWorkout(
      PR2ID,
      test_user_2_username,
      test_user_password
    );
    coachWorkout = await retrieveWorkout(
      CO2ID,
      test_user_2_username,
      test_user_password
    );

    coachPrivateWorkout = await retrieveWorkout(
      PR1ID,
      test_user_username,
      test_user_password
    );
    coachCoachWorkout = await retrieveWorkout(
      CO1ID,
      test_user_username,
      test_user_password
    );
  });

  // ****************** TEST1 - check public *********************

  it("Retrive public workout by test_user_2, check id", async () => {
    const workout = await retrieveWorkout(
      publicWorkout.id,
      test_user_2_username,
      test_user_password
    );
    expect(workout).to.have.property("id", publicWorkout.id);
  });

  it("Retrive workouts by test_user_2, check if new public workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_2_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, publicWorkout)).to.eq(
      true,
      "Did not find workout"
    );
  });

  it("As test_user_2, check if public workout has file", async () => {
    expect(publicWorkout.files[0].file).to.eq(
      fileStart + publicWorkout.id + fileEnd
    );
  });

  it("As test_user_2, check public workout-files", async () => {
    baseOptionsGet.url = url + "api/workout-files/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const files = await testRequest(baseOptionsGet, 200);
    expect(checkIfIdIncluded(files.results, PU2FileID)).to.eq(
      true,
      "Did not find file"
    );
  });

  it("As test_user_2, check public workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PU2FileID;
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const file = await testRequest(baseOptionsGet, 200);
    expect(file.file).to.eq(fileStart + publicWorkout.id + fileEnd);
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: publicWorkout.url,
    };
    login(test_user_2_username, test_user_password, baseOptionsComments);
    const comment = await testRequest(baseOptionsComments, 201);

    baseOptionsGet.url = url + "api/comments/" + comment.id + "/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    await testRequest(baseOptionsGet, 200);

    baseOptionsGet.url = url + "api/comments/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const retrievedComments = await testRequest(baseOptionsGet, 200);
    expect(checkIfIncluded(retrievedComments.results, comment)).to.eq(
      true,
      "Did not find comment"
    );
  });

  // ****************** TEST2 - check private *********************

  it("Retrive private workout by test_user_2, check id", async () => {
    const workout = await retrieveWorkout(
      privateWorkout.id,
      test_user_2_username,
      test_user_password
    );
    expect(workout).to.have.property("id", privateWorkout.id);
  });

  it("Retrive workouts by test_user_2, check if new private workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_2_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, privateWorkout)).to.eq(
      true,
      "Did not find workout"
    );
  });

  it("As test_user_2, check if private workout has file", async () => {
    expect(privateWorkout.files[0].file).to.eq(
      fileStart + privateWorkout.id + fileEnd
    );
  });

  it("As test_user_2, check private workout-files", async () => {
    baseOptionsGet.url = url + "api/workout-files/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const files = await testRequest(baseOptionsGet, 200);
    expect(checkIfIdIncluded(files.results, PR2FileID)).to.eq(
      true,
      "Did not find file"
    );
  });

  it("As test_user_2, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PR2FileID;
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const file = await testRequest(baseOptionsGet, 200);
    expect(file.file).to.eq(fileStart + privateWorkout.id + fileEnd);
  });

  it("Make and retrive comment from private workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: privateWorkout.url,
    };
    login(test_user_2_username, test_user_password, baseOptionsComments);
    const comment = await testRequest(baseOptionsComments, 201);

    baseOptionsGet.url = url + "api/comments/" + comment.id + "/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    await testRequest(baseOptionsGet, 200);

    baseOptionsGet.url = url + "api/comments/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const retrievedComments = await testRequest(baseOptionsGet, 200);
    expect(checkIfIncluded(retrievedComments.results, comment)).to.eq(
      true,
      "Did not find comment"
    );
  });

  // ****************** TEST3 - check coach *********************

  it("Retrive coach workout by test_user_2, check id", async () => {
    const workout = await retrieveWorkout(
      coachWorkout.id,
      test_user_2_username,
      test_user_password
    );
    expect(workout).to.have.property("id", coachWorkout.id);
  });

  it("Retrive workouts by test_user_2, check if new coach workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_2_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, coachWorkout)).to.eq(
      true,
      "Did not find workout"
    );
  });

  it("As test_user_2, check if coach workout has file", async () => {
    expect(coachWorkout.files[0].file).to.eq(
      fileStart + coachWorkout.id + fileEnd
    );
  });

  it("As test_user_2, check coach workout-files", async () => {
    baseOptionsGet.url = url + "api/workout-files/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const files = await testRequest(baseOptionsGet, 200);
    expect(checkIfIdIncluded(files.results, CO2FileID)).to.eq(
      true,
      "Did not find file"
    );
  });

  it("As test_user_2, check coach workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + CO2FileID;
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const file = await testRequest(baseOptionsGet, 200);
    expect(file.file).to.eq(fileStart + coachWorkout.id + fileEnd);
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: coachWorkout.url,
    };
    login(test_user_2_username, test_user_password, baseOptionsComments);
    const comment = await testRequest(baseOptionsComments, 201);

    baseOptionsGet.url = url + "api/comments/" + comment.id + "/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    await testRequest(baseOptionsGet, 200);

    baseOptionsGet.url = url + "api/comments/";
    login(test_user_2_username, test_user_password, baseOptionsGet);
    const retrievedComments = await testRequest(baseOptionsGet, 200);
    expect(checkIfIncluded(retrievedComments.results, comment)).to.eq(
      true,
      "Did not find comment"
    );
  });

  // ****************** TEST4 - is coach check public *********************

  it("Retrive public workout by test_user_2, check id", async () => {
    const workout = await retrieveWorkout(
      publicWorkout.id,
      test_user_username,
      test_user_password
    );
    expect(workout).to.have.property("id", publicWorkout.id);
  });

  it("Retrive workouts by test_user_2, check if new public workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, publicWorkout)).to.eq(
      true,
      "Did not find workout"
    );
  });

  it("As test_user, check if public workout has file", async () => {
    const workout = await retrieveWorkout(
      publicWorkout.id,
      test_user_username,
      test_user_password
    );
    expect(workout.files[0].file).to.eq(fileStart + publicWorkout.id + fileEnd);
  });

  it("As test_user, check public workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PU2FileID;
    login(test_user_username, test_user_password, baseOptionsGet);
    const file = await testRequest(baseOptionsGet, 200);
    expect(file.file).to.eq(fileStart + publicWorkout.id + fileEnd);
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: publicWorkout.url,
    };
    login(test_user_username, test_user_password, baseOptionsComments);
    const comment = await testRequest(baseOptionsComments, 201);

    baseOptionsGet.url = url + "api/comments/" + comment.id + "/";
    login(test_user_username, test_user_password, baseOptionsGet);
    await testRequest(baseOptionsGet, 200);

    baseOptionsGet.url = url + "api/comments/";
    login(test_user_username, test_user_password, baseOptionsGet);
    const retrievedComments = await testRequest(baseOptionsGet, 200);
    expect(checkIfIncluded(retrievedComments.results, comment)).to.eq(
      true,
      "Did not find comment"
    );
  });

  // ****************** TEST5 - is coach check private *********************

  it("Retrive private workout by test_user2", async () => {
    await retrieveWorkout(
      privateWorkout.id,
      test_user_username,
      test_user_password,
      403
    );
  });

  it("Retrive workouts by test_user, check if new private workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, privateWorkout)).to.eq(
      false,
      "Workout should not have been included"
    );
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PR2FileID;
    login(test_user_username, test_user_password, baseOptionsGet);
    await testRequest(
      baseOptionsGet,
      403,
      "Should not be able to retrieve this file"
    );
  });

  // it("Make comment from private workout by test_user2", async () => {
  //   baseOptionsComments.body = {
  //     content: "test comment",
  //     workout: privateWorkout.url,
  //   };
  //   login(test_user_username, test_user_password, baseOptionsComments);
  //   await testRequest(
  //     baseOptionsComments,
  //     401,
  //     "Should not be able to comment on another user's private workout"
  //   );
  // });

  // ****************** TEST6 - is coach check coach *********************

  it("Retrive coach workout by test_user2, check id", async () => {
    const workout = await retrieveWorkout(
      coachWorkout.id,
      test_user_username,
      test_user_password
    );
    expect(workout).to.have.property("id", coachWorkout.id);
  });

  it("Retrive workouts by test_user2, check if new coach workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, coachWorkout)).to.eq(
      true,
      "Did not find workout"
    );
  });

  it("As test_user, check if public workout has file", async () => {
    const workout = await retrieveWorkout(
      coachWorkout.id,
      test_user_username,
      test_user_password
    );
    expect(workout.files[0].file).to.eq(fileStart + coachWorkout.id + fileEnd);
  });

  it("As test_user, check public workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + CO2FileID;
    login(test_user_username, test_user_password, baseOptionsGet);
    const file = await testRequest(baseOptionsGet, 200);
    expect(file.file).to.eq(fileStart + coachWorkout.id + fileEnd);
  });

  it("Make and retrive comment from coach workout by test_user2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: coachWorkout.url,
    };
    login(test_user_username, test_user_password, baseOptionsComments);
    const comment = await testRequest(baseOptionsComments, 201);

    baseOptionsGet.url = url + "api/comments/" + comment.id + "/";
    login(test_user_username, test_user_password, baseOptionsGet);
    await testRequest(baseOptionsGet, 200);

    baseOptionsGet.url = url + "api/comments/";
    login(test_user_username, test_user_password, baseOptionsGet);
    const retrievedComments = await testRequest(baseOptionsGet, 200);
    expect(checkIfIncluded(retrievedComments.results, comment)).to.eq(
      true,
      "Did not find comment"
    );
  });

  // ****************** TEST7 - is not logged in check public *********************

  it("Retrive public workout by test_user_2, check id", async () => {
    baseOptionsGet.url = url + "api/workouts/" + publicWorkout.id + "/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("Retrive workouts by test_user_2, check if new public workout is retrieved", async () => {
    baseOptionsGet.url = url + "api/workouts/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PU2FileID;
    await testRequest(
      baseOptionsGet,
      401,
      "Should not be able to retrieve this file"
    );
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: publicWorkout.url,
    };
    baseOptionsComments.auth = null;
    const comment = await testRequest(
      baseOptionsComments,
      401,
      "should not be able to make comment"
    );
  });

  // ****************** TEST8 - is not logged in check private *********************

  it("Retrive public workout by test_user_2, check id", async () => {
    baseOptionsGet.url = url + "api/workouts/" + privateWorkout.id + "/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("Retrive workouts by test_user_2, check if new public workout is retrieved", async () => {
    baseOptionsGet.url = url + "api/workouts/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PR2FileID;
    await testRequest(
      baseOptionsGet,
      401,
      "Should not be able to retrieve this file"
    );
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: privateWorkout.url,
    };
    baseOptionsComments.auth = null;
    const comment = await testRequest(
      baseOptionsComments,
      401,
      "should not be able to make comment"
    );
  });

  // ****************** TEST9 - is not logged in check coach *********************

  it("Retrive public workout by test_user_2, check id", async () => {
    baseOptionsGet.url = url + "api/workouts/" + coachWorkout.id + "/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("Retrive workouts by test_user_2, check if new public workout is retrieved", async () => {
    baseOptionsGet.url = url + "api/workouts/";
    baseOptionsGet.auth = null;
    await testRequest(baseOptionsGet, 401);
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + CO2FileID;
    await testRequest(
      baseOptionsGet,
      401,
      "Should not be able to retrieve this file"
    );
  });

  it("Make and retrive comment from public workout by test_user_2", async () => {
    baseOptionsComments.body = {
      content: "test comment",
      workout: coachWorkout.url,
    };
    baseOptionsComments.auth = null;
    const comment = await testRequest(
      baseOptionsComments,
      401,
      "should not be able to make comment"
    );
  });

  // ****************** TEST10 - is athlete check private *********************

  it("Retrive private workout by test_user", async () => {
    await retrieveWorkout(
      coachPrivateWorkout.id,
      test_user_2_username,
      test_user_password,
      403
    );
  });

  it("Retrive workouts by test_user, check if new private workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_2_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, coachPrivateWorkout)).to.eq(
      false,
      "Workout should not have been included"
    );
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + PR1FileID;
    login(test_user_2_username, test_user_password, baseOptionsGet);
    await testRequest(
      baseOptionsGet,
      403,
      "Should not be able to retrieve this file"
    );
  });

  // it("Make comment from private workout by test_user2", async () => {
  //   baseOptionsComments.body = {
  //     content: "test comment",
  //     workout: coachPrivateWorkout.url,
  //   };
  //   login(test_user_2_username, test_user_password, baseOptionsComments);
  //   await testRequest(
  //     baseOptionsComments,
  //     401,
  //     "Should not be able to comment on another user's private workout"
  //   );
  // });

  // ****************** TEST11 - is athlete check coach *********************

  it("Retrive coach workout by test_user", async () => {
    await retrieveWorkout(
      coachCoachWorkout.id,
      test_user_2_username,
      test_user_password,
      403
    );
  });

  it("Retrive workouts by test_user, check if new private workout is retrieved", async () => {
    const workouts = await retrieveWorkouts(
      test_user_2_username,
      test_user_password
    );
    expect(checkIfIncluded(workouts.results, coachCoachWorkout)).to.eq(
      false,
      "Workout should not have been included"
    );
  });

  it("As test_user, check private workout-files/id", async () => {
    baseOptionsGet.url = url + "api/workout-files/" + CO1FileID;
    login(test_user_2_username, test_user_password, baseOptionsGet);
    await testRequest(
      baseOptionsGet,
      403,
      "Should not be able to retrieve this file"
    );
  });

  // it("Make comment from private workout by test_user2", async () => {
  //   baseOptionsComments.body = {
  //     content: "test comment",
  //     workout: coachCoachWorkout.url,
  //   };
  //   login(test_user_2_username, test_user_password, baseOptionsComments);
  //   await testRequest(
  //     baseOptionsComments,
  //     401,
  //     "Should not be able to comment on another user's private workout"
  //   );
  // });
});
