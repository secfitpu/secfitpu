describe("Integration tests for user profile and weekly statistics", () => {
  const url = "http://localhost:8000/";

  const test_user_username = "test_user";
  const test_user_password = "test_password";
  const test_user_id = 1;
  const test_user2_id = 2;
  const test_user3_id = 3;

  // Base options object for HTTP requests
  const baseOptions = {
    url: url,
    method: "GET",
    auth: null,
    failOnStatusCode: false,
  };

  const checkOwnerIdOfResults = (results, id) => {
    const owner_ids = results.map((res) => res.owner_id);
    return owner_ids.every((owner_id) => owner_id == id);
  };

  before(() => {
    // Get access token
    const body = { username: test_user_username, password: test_user_password };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      baseOptions.auth = { bearer: accessToken };
    });
  });

  it("Get profile information for logged in user", () => {
    const options = Object.assign({}, baseOptions);
    options.url += "api/user-profiles/?user=current";
    cy.request(options).then((resp) => {
      expect(resp.status).to.eq(200, "HTTP status code");
      expect(resp.body.results[0].username).to.eq(
        test_user_username,
        "Username of returned user"
      );
      expect(resp.body.count).to.eq(1, "Only the current user is returned");
    });
  });

  it("Get profile information for all users", () => {
    const options = Object.assign({}, baseOptions);
    options.url += "api/user-profiles/";
    cy.request(options).then((resp) => {
      expect(resp.status).to.eq(200, "HTTP status code");
      expect(resp.body.count).to.be.greaterThan(1, "More than 1 user returned");
    });
  });

  it("Get personal workout statistics", () => {
    const options = Object.assign({}, baseOptions);
    options.url += "api/workout-statistics/?user=current";
    cy.request(options).then((resp) => {
      expect(resp.status).to.eq(200, "HTTP status code");
      expect(checkOwnerIdOfResults(resp.body.results, test_user_id)).to.eq(
        true,
        "Owner of all returned workouts"
      );
      const visibility = resp.body.results.map((res) => res.visibility);
      expect(visibility).to.include("PUBLIC", "Public workout visible");
      expect(visibility).to.include("COACH", "Coach level workout visible");
      expect(visibility).to.include("PRIVATE", "Private workout visible");
    });
  });

  it("Get athlete workout statistics when coach", () => {
    const options = Object.assign({}, baseOptions);
    options.url += "api/workout-statistics/?user=" + test_user2_id;
    cy.request(options).then((resp) => {
      expect(resp.status).to.eq(200, "HTTP status code");
      expect(checkOwnerIdOfResults(resp.body.results, test_user2_id)).to.eq(
        true,
        "Correct owner_id of all returned workouts"
      );
      const visibility = resp.body.results.map((res) => res.visibility);
      expect(visibility).to.include("PUBLIC", "Public workout visible");
      expect(visibility).to.include("COACH", "Coach level workout visible");
      expect(visibility).to.not.include(
        "PRIVATE",
        "Private workout not visible"
      );
    });
  });

  it("Get public workout statistics", () => {
    const options = Object.assign({}, baseOptions);
    options.url += "api/workout-statistics/?user=" + test_user3_id;
    cy.request(options).then((resp) => {
      expect(resp.status).to.eq(200, "HTTP status code");
      expect(checkOwnerIdOfResults(resp.body.results, test_user3_id)).to.eq(
        true,
        "Correct owner_id of all returned workouts"
      );
      const visibility = resp.body.results.map((res) => res.visibility);
      expect(visibility).to.include("PUBLIC", "Public workout visible");
      expect(visibility).to.not.include(
        "COACH",
        "Coach level workout not visible"
      );
      expect(visibility).to.not.include(
        "PRIVATE",
        "Private workout not visible"
      );
    });
  });
});
