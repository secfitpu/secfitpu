describe("Exercises", () => {
  const url = "http://localhost:8000/";

  const test_user_username = "test_user";
  const test_user_password = "test_password";

  // valid values for exercise
  const baseExercise= {
    name: "Running",
    description: "You use your legs to walk fast",
    unit: "km",
  };

  // Base options object for HTTP request 
  const baseOptions = {
    url: url + "api/exercises/",
    method: null,
    auth: null,
    body: baseExercise,
    failOnStatusCode: false,
  };

  function login(username, password, options){
    const body = { username: username, password: password };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      options.auth = { bearer: accessToken };
    });
  }

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };


  function checkIfIdIncluded(list, id){
    var boolean = false;
    list.forEach( listElem => {
      if(listElem.id === id){
        boolean = true;
        return;
      }
    });
    return boolean;
  }

  it("Retrive exercises", async () => {
    baseOptions.method = "GET";
    login(test_user_username, test_user_password, baseOptions);
    const exercises = await testRequest(baseOptions, 200);
    expect(checkIfIdIncluded(exercises.results, 1)).to.eq(true, "Did not find exercise with id 1");
  });

  it("Post, edit and delete an exercise", async () => {
    baseOptions.method = "POST";
    login(test_user_username, test_user_password, baseOptions);
    const exercise = await testRequest(baseOptions, 201);

    baseOptions.method = "PUT";
    baseOptions.url = baseOptions.url + exercise.id + "/";
    await testRequest(baseOptions, 200);

    baseOptions.method = "DELETE";
    await testRequest(baseOptions, 204);
  });
});
