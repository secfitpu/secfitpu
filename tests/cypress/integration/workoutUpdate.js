describe("Update workout", () => {
  const url = "http://localhost:8000/";

  const baseWorkout = {
    name: "name",
    date: "2021-02-28T23:00:00.000Z",
    notes: "notes",
    visibility: "PUBLIC",
    exercise_instances: [
      {
        exercise: url + "api/exercises/1/",
        repetitions: 10,
        sets: 1,
      },
    ],
  };

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/workouts/1/",
    method: "PUT",
    auth: null,
    body: baseWorkout,
    failOnStatusCode: false,
  };

  before(() => {
    // Get access token
    const body = { username: "test_user", password: "test_password" };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      baseOptions.auth = { bearer: accessToken };
    });
  });

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  it("Update workout", async () => {
    const workout = await testRequest(baseOptions, 200);
    cy.expect(workout.name).to.eql(baseWorkout.name);
    cy.expect(workout.dates).to.eql(baseWorkout.dates);
    cy.expect(workout.notes).to.eql(baseWorkout.notes);
    cy.expect(workout.visibility).to.eql(baseWorkout.visibility);
    cy.expect(workout.exercise_instances[0].exercise).to.eql(
      baseWorkout.exercise_instances[0].exercise
    );
    cy.expect(workout.exercise_instances[0].number).to.eql(
      baseWorkout.exercise_instances[0].number
    );
    cy.expect(workout.exercise_instances[0].sets).to.eql(
      baseWorkout.exercise_instances[0].sets
    );
  });

  it("Update workout, new name", async () => {
    baseWorkout.name = "Updated Test user 1 public workout";
    const workout = await testRequest(baseOptions, 200);
    cy.expect(workout.name).to.eql("Updated Test user 1 public workout");
  });

  it("Update workout, remove exercises", async () => {
    baseWorkout.exercise_instances = [];
    const workout = await testRequest(baseOptions, 200);
    cy.expect(workout.exercise_instances).to.eql([]);
  });

  it("Update workout, add exercises", async () => {
    baseWorkout.exercise_instances = [
      {
        exercise: url + "api/exercises/1/",
        repetitions: 10,
        sets: 1,
      },
      {
        exercise: url + "api/exercises/1/",
        repetitions: 10,
        sets: 1,
      },
    ];
    const workout = await testRequest(baseOptions, 200);
    cy.expect(workout.exercise_instances.length).to.eql(2);
  });
});
