describe("Workout boundary tests", () => {
  const url = "http://localhost:8000/";

  // valid values for workout
  const baseWorkout = {
    name: "name",
    date: "2021-02-28T23:00:00.000Z",
    notes: "notes",
    visibility: "PRIVATE",
    exercise_instances: [
      {
        exercise: url + "api/exercises/1/",
        repetitions: "10",
        sets: "1",
      },
    ],
  };

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/workouts/",
    method: "POST",
    auth: null,
    body: baseWorkout,
    failOnStatusCode: false,
  };

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body.id);
        } catch (error) {
          resolve();
        }
      });
    });
  };

  before(() => {
    // Get access token
    const body = { username: "test_user", password: "test_password" };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      baseOptions.auth = { bearer: accessToken };
    });
  });

  it("Send base workout", () => {
    testRequest(baseOptions, 201);
  });

  it("Name boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions)); // deep copy

    options.body.name = "";
    await testRequest(options, 400);

    options.body.name = null;
    await testRequest(options, 400);

    options.body.name = 2;
    await testRequest(options, 201);

    options.body.name = true;
    await testRequest(options, 400);

    options.body.name =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempor eget purus a tristique. Et"; // 100 characters
    await testRequest(options, 201);

    options.body.name =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempor eget purus a tristique. Eti"; // 101 characters
    await testRequest(options, 400);
  });

  it("Date boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.date = "";
    await testRequest(options, 400);

    options.body.date = null;
    await testRequest(options, 400);

    options.body.date = "2021-02-28T23";
    await testRequest(options, 400);

    options.body.date = "0001-01-01T00:00:00.000Z";
    await testRequest(options, 201);

    options.body.date = "0000-01-01T00:00:00.000Z";
    await testRequest(options, 400);

    options.body.date = "9999-12-31T23:59:59.999Z";
    await testRequest(options, 201);

    options.body.date = "10000-01-01T00:00:00.000Z";
    await testRequest(options, 400);
  });

  it("Notes boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.notes = "";
    await testRequest(options, 400);

    options.body.notes = null;
    await testRequest(options, 400);

    options.body.notes = 2;
    await testRequest(options, 201);

    options.body.notes = true;
    await testRequest(options, 400);

    options.body.notes = "A1";
    await testRequest(options, 201);

    options.body.notes = "[1";
    await testRequest(options, 201);

    options.body.notes = "Aβ";
    await testRequest(options, 201);
  });

  it("Visibility boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.visibility = "PUBLIC";
    await testRequest(options, 201);

    options.body.visibility = "COACH";
    await testRequest(options, 201);

    options.body.visibility = "PRIVATE";
    await testRequest(options, 201);

    options.body.visibility = "Pu";
    await testRequest(options, 400);

    options.body.visibility = "pu";
    await testRequest(options, 400);

    options.body.visibility = "P";
    await testRequest(options, 400);

    options.body.visibility = "";
    await testRequest(options, 400);

    options.body.visibility = null;
    await testRequest(options, 400);

    options.body.visibility = "PU";
    await testRequest(options, 400);

    options.body.visibility = 2;
    await testRequest(options, 400);
  });

  it("Exercise instance boundary test", async () => {
    const options = JSON.parse(JSON.stringify(baseOptions));

    options.body.exercise_instances = [
      {
        exercise: "http://localhost:8000/api/exercises/1/",
        repetitions: "1",
        sets: "1",
      },
    ];
    await testRequest(options, 201);

    options.body.exercise_instances = [
      {
        exercise: "http://localhost:8000/api/exercises/1/",
        repetitions: "0",
        sets: "0",
      },
    ];
    await testRequest(options, 201);

    options.body.exercise_instances = [
      {
        exercise: "http://localhost:8000/api/exercises/1/",
        repetitions: 1,
        sets: 1,
      },
    ];
    await testRequest(options, 201);

    options.body.exercise_instances = [
      {
        exercise: "http://localhost:8000/api/exercises/1/",
        repetitions: "-1",
        sets: "-1",
      },
    ];
    await testRequest(
      options,
      400,
      "repetitions and sets should not allow negative numbers"
    );

    options.body.exercise_instances = [
      {
        exercise: "1",
        repetitions: "1",
        sets: "1",
      },
    ];
    await testRequest(options, 400);

    options.body.exercise_instances = [
      {
        exercise: "http://localhost:8000/api/exercises/1000/",
        repetitions: 1,
        sets: 1,
      },
    ];
    await testRequest(options, 400);
  });
});
