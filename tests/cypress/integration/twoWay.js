describe("Two way domain test on registration page", () => {
  const url = "http://localhost:8000/";

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/users/",
    method: "POST",
    auth: null,
    body: {
      username: "name",
      email: null,
      password: "test_password",
      password1: "test_password",
      phone_number: null,
      country: null,
      city: null,
      street_address: null,
    },
    failOnStatusCode: false,
    form: true,
  };

  const randomString = (radix, length) => {
    // generate randoms string. Max length: 11
    // radix=10 => random number
    // radix=36 => all numbers and a-z lowercase letters
    return Math.random().toString(radix).substr(2, length);
  };

  // valid values for creating user
  const inputDomains = [
    [["name", 201]], // username
    [
      ["test@mail.com", 201],
      ["test", 400],
    ], // email
    [
      ["test_password", 201],
      ["pass", 400],
    ], // password
    [
      ["12345678", 201],
      ["number", 400],
    ], // phone number
    [
      ["country", 201],
      ["country1", 400],
    ], // country
    [["city", 201]], // city
    [["Street 1", 201]], // street adress
  ];
  const fields = ["username", "email", "password", "phone_number", "country", "city", "street_address"];

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body.id);
        } catch (error) {
          resolve();
        }
      });
    });
  };

  it("Test all combinations", async () => {
    for (let i = 0; i < inputDomains.length - 1; i++) {
      for (let j = 0; j < inputDomains[i].length; j++) {
        for (let k = i + 1; k < inputDomains.length; k++) {
          for (let l = 0; l < inputDomains[k].length; l++) {
            const options = JSON.parse(JSON.stringify(baseOptions)); // deep copy
            if ((i == 2) | (k == 2)) {
              // if password tested
              options.body.password1 = i == 2 ? inputDomains[i][j][0] : inputDomains[k][l][0]; // set password1 equal to password
            }
            options.body[fields[i]] = inputDomains[i][j][0]; // set first test variable
            options.body[fields[k]] = inputDomains[k][l][0]; // set second test variable
            options.body.username = randomString(36, 7); // random string [a-z,0-9] of length 7 to avoid duplicate usernames
            const testMessage = fields[i] + j + " " + fields[k] + l;
            const expectedStatus = Math.max(inputDomains[i][j][1], inputDomains[k][l][1]);
            await testRequest(options, expectedStatus, testMessage);
          }
        }
      }
    }
  });
});
