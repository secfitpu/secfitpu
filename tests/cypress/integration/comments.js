describe("Comments", () => {
  const url = "http://localhost:8000/";

  // valid values for workout
  const baseComment = {
    owner: "test_user_2",
    timestamp: "2021-02-28T23:00:00.000Z",
    content: "Test comment",
    workout: url + "api/workouts/4/",
  };

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/comments/",
    method: null,
    auth: null,
    body: baseComment,
    failOnStatusCode: false,
  };


  before(() => {
    // Get access token
    const body = { username: "test_user_2", password: "test_password" };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      baseOptions.auth = { bearer: accessToken };
    });
  });

  function login(username, password, options) {
    const body = { username: username, password: password };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      options.auth = { bearer: accessToken };
    });
  }

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  it("Make comment", async () => {
    baseOptions.method = "POST";
    const comment = await testRequest(baseOptions, 201);
    console.log(comment);
    cy.expect(comment.owner).to.eql(baseComment.owner);
    cy.expect(comment.content).to.eql(baseComment.content);
    cy.expect(comment.workout).to.eql(baseComment.workout);
  });

  it("Retrieve comment", async () => {
    baseOptions.method = "GET";
    const comments = await testRequest(baseOptions, 200);

    var boolean = false;
    comments.results.forEach( comment => {
      if(
          comment.content === baseComment.content &&
          comment.owner === baseComment.owner &&
          comment.workout === baseComment.workout
        ){
        boolean = true;
      }
    });
    cy.expect(boolean).to.eql(true);
  });

  it("Try to retrieve comment as coach test_user", async () => {
    baseOptions.method = "GET";
    login("test_user", "test_password", baseOptions)
    const comments = await testRequest(baseOptions, 200);

    var boolean = false;
    comments.results.forEach( comment => {
      if(
          comment.content === baseComment.content &&
          comment.owner === baseComment.owner &&
          comment.workout === baseComment.workout
        ){
        boolean = true;
      }
    });
    cy.expect(boolean).to.eql(true);
  });

  it("Try to retrieve comment as test_user_3", async () => {
    baseOptions.method = "GET";
    login("test_user_3", "test_password", baseOptions)
    const comments = await testRequest(baseOptions, 200);

    var boolean = true;
    comments.results.forEach( comment => {
      if(
          comment.content === baseComment.content &&
          comment.owner === baseComment.owner &&
          comment.workout === baseComment.workout
        ){
        boolean = false;
      }
    });
    cy.expect(boolean).to.eql(true);
  });
});
