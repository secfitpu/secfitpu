describe("Athlete page tests", () => {
  before(() => {
    cy.visit("/login.html"); // go to baseUrl, baseUrl defined in cypress.json
    cy.intercept("/api/*").as("getData"); // create route getData

    // Log in user
    cy.get("#form-login input").eq(0).type("test_user"); // username input
    cy.get("#form-login input").eq(1).type("test_password"); // password input
    cy.get("#btn-login").click();
    cy.wait("@getData");
  });

  it("Athlete workout statiscits loaded", () => {
    cy.visit("/myathletes.html");
    cy.intercept("api/users/2").as("getAthlete");
    cy.intercept("api/workout-statistics").as("getStatistics");

    cy.wait("@getAthlete");
    cy.get("#tab-test_user_2").click();
    cy.wait("@getStatistics");
    cy.get(".highcharts-figure");
    cy.get(".highcharts-point");
  });

  it("Check upload form", () => {
    cy.get("#nav-tabContent .input-group>input").should("have.length", 2);
  });

  it("Check buttons", () => {
    cy.get("#btn-next-test_user_2").should('be.disabled');
    cy.get("#btn-prev-test_user_2").should('be.disabled');
  });
});
