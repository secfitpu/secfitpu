describe("Remember_Me", () => {
  const url = "http://localhost:8000/";

  const test_user_username = "test_user";
  const test_user_password = "test_password";

  // Base options object for HTTP request 
  const baseOptions = {
    url: url + "api/remember_me/",
    method: null,
    auth: null,
    failOnStatusCode: false,
  };

  function login(username, password, options){
    const body = { username: username, password: password };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      options.auth = { bearer: accessToken };
    });
  }

  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  it("GET and POST remember_me", async () => {
    baseOptions.method = "GET";
    login(test_user_username, test_user_password, baseOptions);
    const response = await testRequest(baseOptions, 200);

    baseOptions.auth = null;
    baseOptions.headers = {
      "Cookie" : "remember_me="+response.remember_me
    };
    baseOptions.method = "POST";
    await testRequest(baseOptions, 200); 
  });
});
