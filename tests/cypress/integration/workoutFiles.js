describe("Retrieve workout files", () => {
  const url = "http://localhost:8000/";

  const baseWorkoutFile = {
    url: url + "api/workout-files/3/",
    id: 3,
    owner: "test_user",
    file: url + "media/workouts/6/file_test.txt",
    workout: url + "api/workouts/6/"
  };

  // Base options object for HTTP requests
  const baseOptions = {
    url: url + "api/workout-files/",
    method: "GET",
    auth: null,
    body: baseWorkoutFile,
    failOnStatusCode: false,
  };


  before(() => {
    // Get access token
    const body = { username: "test_user", password: "test_password" };
    cy.request("POST", url + "api/token/", body).then((resp) => {
      expect(resp.status).to.eq(200);
      const accessToken = resp.body.access;
      baseOptions.auth = { bearer: accessToken };
    });
  });


  const testRequest = (options, expectedStatus, message) => {
    return new Promise((resolve, reject) => {
      cy.request(options).then((resp) => {
        try {
          expect(resp.status).to.eq(expectedStatus, message);
          resolve(resp.body);
        } catch (error) {
          reject(error);
        }
      });
    });
  };

  it("Retrieve workout files", async () => {
    const workoutFiles = await testRequest(baseOptions, 200);
    cy.expect(workoutFiles.length).not.to.eql(0);
  });

  it("Retrieve workout file 3", async () => {
    baseOptions.url = url + "api/workout-files/3/";
    const workoutFile = await testRequest(baseOptions, 200);
    cy.expect(workoutFile.url).to.eql(baseWorkoutFile.url);
    cy.expect(workoutFile.id).to.eql(baseWorkoutFile.id);
    cy.expect(workoutFile.owner).to.eql(baseWorkoutFile.owner);
    cy.expect(workoutFile.file).to.eql(baseWorkoutFile.file);
    cy.expect(workoutFile.workout).to.eql(baseWorkoutFile.workout);
  });

  it("Try to update workout file 3", async () => {
    baseOptions.method = "PUT"
    const response = await testRequest(baseOptions, 405);
    cy.expect(response.detail).to.eql('Method "PUT" not allowed.');
  });
});
