from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import AthleteFile, Offer


class CustomUserAdmin(UserAdmin):
    custom_user_admin_fields = ((None, {"fields": ("coach",)}),)

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = get_user_model()
    fieldsets = UserAdmin.fieldsets + custom_user_admin_fields
    add_fieldsets = UserAdmin.add_fieldsets + custom_user_admin_fields


admin.site.register(get_user_model(), CustomUserAdmin)
admin.site.register(Offer)
admin.site.register(AthleteFile)
