from django.urls import path

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from users import views


urlpatterns = [
    path("users/", views.UserList.as_view(), name="user-list"),
    path(
        "users/<int:pk>/",
        views.UserDetail.as_view(),
        name="user-detail"
    ),
    path(
        "users/<str:username>/",
        views.UserDetail.as_view(),
        name="user-detail"
    ),
    path(
        "user-profiles/",
        views.UserProfile.as_view(),
        name="user-profile"
    ),
    path("offers/", views.OfferList.as_view(), name="offer-list"),
    path(
        "offers/<int:pk>/",
        views.OfferDetail.as_view(),
        name="offer-detail"
    ),
    path(
        "athlete-files/",
        views.AthleteFileList.as_view(),
        name="athlete-file-list"
    ),
    path(
        "athlete-files/<int:pk>/",
        views.AthleteFileDetail.as_view(),
        name="athletefile-detail",
    ),
    path(
        "token/",
        TokenObtainPairView.as_view(),
        name="token_obtain_pair"
    ),
    path(
        "token/refresh/",
        TokenRefreshView.as_view(),
        name="token_refresh"
    ),
    path(
        "remember_me/",
        views.RememberMe.as_view(),
        name="remember_me"
    ),
]
