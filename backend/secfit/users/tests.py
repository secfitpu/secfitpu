from django.contrib.auth import get_user_model
from django.test import TestCase

from rest_framework import serializers
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework.views import APIView

from . import permissions
from .models import AthleteFile
from .serializers import UserSerializer

TEST_PASSWORD = "TestPassord"
TEST_EMAIL = "bruker@test.no"
TEST_USERNAME = "testbruker"
TEST_PHONE_NUMBER = "11111111"
TEST_COUNTRY = "Norway"
TEST_CITY = "Testby"
TEST_STREET_ADDRESS = "Testgata 42"


# Test Case for UserSerializer
class UserSerializerTestCase(TestCase):

    def setUp(self):
        # Makes test user Dict
        self.test_user = dict()
        self.test_user['username'] = TEST_USERNAME
        self.test_user['email'] = TEST_EMAIL
        self.test_user['password'] = TEST_PASSWORD
        self.test_user['password1'] = TEST_PASSWORD
        self.test_user['phone_number'] = TEST_PHONE_NUMBER
        self.test_user['country'] = TEST_COUNTRY
        self.test_user['city'] = TEST_CITY
        self.test_user['street_address'] = TEST_STREET_ADDRESS

        # Makes the serialier to be tested with the test user data
        self.serializer = UserSerializer(data=self.test_user)

    def test_validate_password(self):
        try:
            # Checks matching passwords
            self.serializer.validate_password(True)
        except:
            self.fail()

    def test_validate_password_validator(self):
        # Skipped since original code did not have any AUTH_PASSWORD_VALIDATORS
        # If the AUTH_PASSWORD_VALIDATORS in settings.py which is at the moment commented out is added,
        # then this test can be un-commented

        self.test_user['password'] = "Short"
        self.serializer = UserSerializer(data=self.test_user)

        try:
            # Checks if settings validator code works (added validator for min 6 char password, the password "Short" is only 5 chars)
            self.serializer.validate_password(True)
            self.fail()
        except serializers.ValidationError as error:
            self.assertEquals(
                error.detail[0], "This password is too short. It must contain at least 6 characters.")
            pass
        except:
            self.fail() 
        
    def test_validate_password_not_matching_passwords(self):
        # This is commented out because the check for if password == password1 does not exist

        self.test_user['password'] = "NotMatchingPassword"
        self.serializer = UserSerializer(data=self.test_user)

        try:
            # Checks non matching passwords
            self.serializer.validate_password(True)
            self.fail()
        except serializers.ValidationError as error:
            self.assertEquals(error.detail[0], "Passwords must match!")
            pass
        except:
            self.fail()
       
             

    def test_create(self):
        # Checks that user does not exists
        try:
            get_user_model().objects.get(username=TEST_USERNAME)
            self.fail()
        except get_user_model().DoesNotExist:
            pass
        except:
            self.fail()

        # Creates test user
        self.serializer.create(self.test_user)

        # Checks if can retrive new user and that the user retrieved has correct email
        self.assertEquals(get_user_model().objects.get(
            username=TEST_USERNAME).email, TEST_EMAIL)


class IsCoachOrAthleteTestCase(TestCase):
    def setUp(self):
        self.test_coach = get_user_model().objects.create(
            username="Coach",
            password=TEST_PASSWORD
        )
        self.test_athlete = get_user_model().objects.create(
            username="Athlete",
            password=TEST_PASSWORD,
            coach=self.test_coach
        )
        self.factory = APIRequestFactory()
        self.isCoachOrAthlete = permissions.IsCoachOrAthlete()
        self.url = "api/athlete-files/"
        self.post_request = self.factory.post(
            self.url,
            {"athlete": self.url + str(self.test_athlete.id) + "/"},
            format="json"
        )

    def assert_request(self, request, user, assert_method, obj=None):
        force_authenticate(request, user=user)
        initialized_request = APIView().initialize_request(request)
        if obj:
            assert_method(self.isCoachOrAthlete.has_object_permission(
                initialized_request, None, obj
            ))
        else:
            assert_method(self.isCoachOrAthlete.has_permission(
                initialized_request, None
            ))

    def test_has_permission_get_method(self):
        request = self.factory.get(self.url)
        self.assert_request(request, self.test_coach, self.assertTrue)

    def test_has_permission_no_athlete(self):
        request = self.factory.post(
            self.url,
            {"athlete": None},
            format="json"
        )
        self.assert_request(request, self.test_coach, self.assertFalse)

    def test_has_permission_coach(self):
        request = self.post_request
        self.assert_request(request, self.test_coach, self.assertTrue)

    def test_has_permission_athlete(self):
        request = self.post_request
        self.assert_request(request, self.test_athlete, self.assertTrue)

    def test_has_object_permission(self):
        athlete_file = AthleteFile.objects.create(
            athlete=self.test_athlete,
            owner=self.test_coach
        )
        request = self.factory.get(self.url)
        self.assert_request(
            request,
            self.test_coach,
            self.assertTrue,
            athlete_file
        )
        self.assert_request(
            request,
            self.test_athlete,
            self.assertTrue,
            athlete_file
        )

    def test_no_permission(self):
        test_user = get_user_model().objects.create(
            username="Test user",
            password=TEST_PASSWORD
        )
        request = self.post_request
        self.assert_request(request, test_user, self.assertFalse)

        athlete_file = AthleteFile.objects.create(
            athlete=self.test_athlete,
            owner=self.test_coach
        )
        request = self.factory.get(self.url)
        self.assert_request(
            request,
            test_user,
            self.assertFalse,
            athlete_file
        )
