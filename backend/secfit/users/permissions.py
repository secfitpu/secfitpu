from django.contrib.auth import get_user_model
from rest_framework import permissions


class IsCurrentUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsCoachOrAthlete(permissions.BasePermission):
    def has_permission(self, request, view):
        athlete_role = "athlete"
        athlete = get_object_from_request(
            request,
            athlete_role,
            get_user_model()
        )
        if isinstance(athlete, bool):
            return athlete
        return athlete.coach == request.user or athlete == request.user

    def has_object_permission(self, request, view, obj):
        return request.user == obj.athlete.coach or request.user == obj.athlete


def get_object_from_request(request, object_string, model):
    if request.method == "POST":
        if request.data.get(object_string):
            object_id = request.data[object_string].split("/")[-2]
            return model.objects.get(pk=object_id)
        return False
    return True
