"""
Tests for the workouts application.
"""

from django.contrib.auth import get_user_model
from django.test import TestCase
from exercises.models import Exercise, ExerciseInstance
from rest_framework.test import APIRequestFactory, force_authenticate
from rest_framework.views import APIView

from workouts.models import Workout

from . import permissions

TEST_PASSWORD = "TestPassord"
TEST_EMAIL = "bruker@test.no"
TEST_USERNAME = "testbruker"

TEST_WORKOUT_NAME = "Min test workout"
TEST_WORKOUT_DATE = "2021-03-01T10:14:00.000Z"
TEST_WORKOUT_NOTES = "Var en bra workout"
TEST_WORKOUT_VISIBILITY = Workout.PUBLIC

TEST_EXERCISE_NAME = "Running"
TEST_EXERCISE_DESCRIPTION = "You run as fast as you can"
TEST_EXERCISE_UNIT = "test-unit"

TEST_EXERCISE_INSTANCE_SETS = 2
TEST_EXERCISE_INSTANCE_REPETITIONS = 2

TEST_COACH_PASSWORD = TEST_PASSWORD
TEST_COACH_EMAIL = "coach@test.no"
TEST_COACH_USERNAME = "testcoach"


def initializeTestData():
    # Creates test objects from dummy data

    # Make user
    test_user = get_user_model().objects.create(
        username=TEST_USERNAME, email=TEST_EMAIL, password=TEST_PASSWORD)

    # Make coach
    test_coach = get_user_model().objects.create(
        username=TEST_COACH_USERNAME, email=TEST_COACH_EMAIL, password=TEST_COACH_PASSWORD)
    test_user.coach = test_coach

    # Make Exercise
    test_exercise = Exercise.objects.create(
        name=TEST_EXERCISE_NAME, description=TEST_EXERCISE_DESCRIPTION, unit=TEST_EXERCISE_UNIT)

    # Make Workout
    test_workout = Workout.objects.create(
        name=TEST_WORKOUT_NAME, date=TEST_WORKOUT_DATE, notes=TEST_WORKOUT_NOTES, owner=test_user)
    test_workout.visibility = TEST_WORKOUT_VISIBILITY
    test_workout_url = '/api/workouts/' + \
        str(test_workout.id)+'/'

    # Makes exercise instance
    exerciseInstance = ExerciseInstance.objects.create(
        workout=test_workout, exercise=test_exercise, sets=TEST_EXERCISE_INSTANCE_SETS, repetitions=TEST_EXERCISE_INSTANCE_REPETITIONS)

    # Make request factory
    factory = APIRequestFactory()

    return test_user, test_coach, test_exercise, test_workout, test_workout_url, exerciseInstance, factory


class isOwnerTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        # Test for IsOwner with arbitraty request url
        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isOwner = permissions.IsOwner()
        self.assertTrue(isOwner.has_object_permission(
            initializedRequest, None, self.test_workout))


class IsOwnerOfWorkoutTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_permission_get_method(self):
        # Test with get method
        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isOwnerOfWorkout = permissions.IsOwnerOfWorkout()
        self.assertTrue(isOwnerOfWorkout.has_permission(
            initializedRequest, None))

    def test_has_permission_no_workout(self):
        # Test with workout: None
        request = self.factory.post(
            self.test_workout_url, {'workout': None}, format='json')
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isOwnerOfWorkout = permissions.IsOwnerOfWorkout()
        self.assertFalse(isOwnerOfWorkout.has_permission(
            initializedRequest, None))

    def test_has_permission_user_is_owner(self):
        # Tests true for all if-statements
        request = self.factory.post(
            self.test_workout_url, {'workout': self.test_workout_url})
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isOwnerOfWorkout = permissions.IsOwnerOfWorkout()
        self.assertTrue(isOwnerOfWorkout.has_permission(
            initializedRequest, None))

    def test_has_object_permission(self):
        # Makes exercise instance
        exerciseInstance = ExerciseInstance.objects.create(
            workout=self.test_workout, exercise=self.test_exercise, sets=TEST_EXERCISE_INSTANCE_SETS, repetitions=TEST_EXERCISE_INSTANCE_REPETITIONS)

        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isOwnerOfWorkout = permissions.IsOwnerOfWorkout()
        self.assertTrue(isOwnerOfWorkout.has_object_permission(
            initializedRequest, None, exerciseInstance))


class IsCoachAndVisibleToCoachTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_coach)
        initializedRequest = APIView().initialize_request(request)

        isCoachAndVisibleToCoach = permissions.IsCoachAndVisibleToCoach()

        self.assertTrue(isCoachAndVisibleToCoach.has_object_permission(
            initializedRequest, None, self.test_workout))

    def test_has_object_permission_private_workout(self):
        self.test_workout.visibility = Workout.PRIVATE

        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_coach)
        initializedRequest = APIView().initialize_request(request)

        isCoachAndVisibleToCoach = permissions.IsCoachAndVisibleToCoach()

        self.assertFalse(isCoachAndVisibleToCoach.has_object_permission(
            initializedRequest, None, self.test_workout))


class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_coach)
        initializedRequest = APIView().initialize_request(request)

        isCoachAndVisibleToCoach = permissions.IsCoachOfWorkoutAndVisibleToCoach()

        self.assertTrue(isCoachAndVisibleToCoach.has_object_permission(
            initializedRequest, None, self.exerciseInstance))

    def test_has_object_permission_private_workout(self):
        self.test_workout.visibility = Workout.PRIVATE

        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_coach)
        initializedRequest = APIView().initialize_request(request)

        isCoachAndVisibleToCoach = permissions.IsCoachOfWorkoutAndVisibleToCoach()

        self.assertFalse(isCoachAndVisibleToCoach.has_object_permission(
            initializedRequest, None, self.exerciseInstance))


class IsPublicTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        isPublic = permissions.IsPublic()

        self.assertTrue(isPublic.has_object_permission(
            None, None, self.test_workout))


class IsWorkoutPublicTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        isWorkoutPublic = permissions.IsWorkoutPublic()

        self.assertTrue(isWorkoutPublic.has_object_permission(
            None, None, self.exerciseInstance))


class IsReadOnlyTestCase(TestCase):

    def setUp(self):
        self.test_user, self.test_coach, self.test_exercise, self.test_workout, self.test_workout_url, self.exerciseInstance, self.factory = initializeTestData()

    def test_has_object_permission(self):
        request = self.factory.get(self.test_workout_url)
        force_authenticate(request, user=self.test_user)
        initializedRequest = APIView().initialize_request(request)

        isReadOnly = permissions.IsReadOnly()

        self.assertTrue(isReadOnly.has_object_permission(
            initializedRequest, None, None))
