from rest_framework import permissions

from users.permissions import get_object_from_request
from workouts.models import Workout


class IsOwner(permissions.BasePermission):
    """Checks whether the requesting user is also the owner of
    the existing object"""

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsOwnerOfWorkout(permissions.BasePermission):
    """Checks whether the requesting user is also the owner of the
    new or existing object"""

    def has_permission(self, request, view):
        workout_string = "workout"
        workout = get_object_from_request(request, workout_string, Workout)
        if isinstance(workout, bool):
            return workout
        return workout.owner == request.user

    def has_object_permission(self, request, view, obj):
        return obj.workout.owner == request.user


class IsCoachAndVisibleToCoach(permissions.BasePermission):
    """Checks whether the requesting user is the existing object's owner's coach
    and whether the object (workout) has a visibility of Public or Coach.
    """

    def has_object_permission(self, request, view, obj):
        return obj.owner.coach == request.user and (
            obj.visibility == Workout.PUBLIC or obj.visibility == Workout.COACH
        )


class IsCoachOfWorkoutAndVisibleToCoach(permissions.BasePermission):
    """Checks whether the requesting user is the existing workout's owner's coach
    and whether the object has a visibility of Public or Coach.
    """

    def has_object_permission(self, request, view, obj):
        return obj.workout.owner.coach == request.user and (
            obj.workout.visibility == Workout.PUBLIC or
            obj.workout.visibility == Workout.COACH
        )


class IsPublic(permissions.BasePermission):
    """Checks whether the object (workout) has visibility of Public."""

    def has_object_permission(self, request, view, obj):
        return obj.visibility == Workout.PUBLIC


class IsWorkoutPublic(permissions.BasePermission):
    """Checks whether the object's workout has visibility of Public."""

    def has_object_permission(self, request, view, obj):
        return obj.workout.visibility == Workout.PUBLIC


class IsReadOnly(permissions.BasePermission):
    """Checks whether the HTTP request verb is only for retrieving data
    (GET, HEAD, OPTIONS)"""

    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS
