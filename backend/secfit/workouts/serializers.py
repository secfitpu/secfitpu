from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField

from exercises.models import ExerciseInstance
from exercises.serializers import ExerciseInstanceSerializer

from workouts.models import Workout, WorkoutFile


class WorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a WorkoutFile.

    Hyperlinks are used for relationships by default.

    Attributes:
        owner:      The owner (User) of the WorkoutFile,
                    represented by a username. ReadOnly
        workout:    The associate workout for this WorkoutFile,
                    represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(),
        view_name="workout-detail",
        required=False
    )

    class Meta:
        model = WorkoutFile
        fields = ["url", "id", "owner", "file", "workout"]

    def create(self, validated_data):
        return WorkoutFile.objects.create(**validated_data)


class WorkoutStatisticSerializer(serializers.HyperlinkedModelSerializer):
    owner_id = serializers.SerializerMethodField()

    class Meta:
        model = Workout
        fields = [
            "id",
            "name",
            "date",
            "owner_id",
            "visibility"
        ]

    def get_owner_id(self, obj):
        return obj.owner.id


class WorkoutSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Workout.

    Hyperlinks are used for relationships by default. This serializer
    specifies nested serialization since a workout consists of
    WorkoutFiles and ExerciseInstances.

    Attributes:
        owner_username:     Username of the owning User
        exercise_instance:  Serializer for ExericseInstances
        files:              Serializer for WorkoutFiles
    """

    owner_username = serializers.SerializerMethodField()
    exercise_instances = ExerciseInstanceSerializer(many=True, required=True)
    files = WorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Workout
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "exercise_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, WorkoutFiles, and a Workout.

        This is needed to iterate over the files and exercise instances,
        since this serializer is nested.

        Args:
            validated_data: Validated files and exercise_instances

        Returns:
            Workout: A newly created Workout
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        workout = Workout.objects.create(**validated_data)

        for exercise_instance_data in exercise_instances_data:
            ExerciseInstance.objects.create(
                workout=workout, **exercise_instance_data
            )
        for file_data in files_data:
            WorkoutFile.objects.create(
                workout=workout,
                owner=workout.owner,
                file=file_data.get("file")
            )

        return workout

    def update_exercise_instances(
            self,
            exercise_instances,
            exercise_instances_data
    ):
        for exercise_instance, exercise_instance_data in zip(
            exercise_instances.all(), exercise_instances_data
        ):
            super().update(exercise_instance, exercise_instance_data)

    def add_workout_file(self, instance, files_data, index):
        WorkoutFile.objects.create(
            workout=instance,
            owner=instance.owner,
            file=files_data[index].get("file"),
        )

    def add_exercise_instance(self, instance, exercise_instance_data, index):
        exercise_instance_data = exercise_instance_data[index]
        ExerciseInstance.objects.create(
            workout=instance, **exercise_instance_data
        )

    def add_new_objects(
            self,
            instance,
            obj_instance,
            obj_instance_data,
            add_function
    ):
        if len(obj_instance_data) > len(obj_instance.all()):
            for i in range(
                len(obj_instance.all()),
                len(obj_instance_data)
            ):
                add_function(instance, obj_instance_data, i)

    def delete_objects(self, obj_instance, obj_instance_data):
        if len(obj_instance_data) < len(obj_instance.all()):
            for i in range(
                len(obj_instance_data),
                len(obj_instance.all())
            ):
                obj_instance.all()[i].delete()

    def update(self, instance, validated_data):
        """Custom logic for updating a Workout with its ExerciseInstances and Workouts.

        This is needed because each object in both exercise_instances and
        files must be iterated over and handled individually.

        Args:
            instance (Workout): Current Workout object
            validated_data: Contains data for validated fields

        Returns:
            Workout: Updated Workout instance
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        exercise_instances = instance.exercise_instances

        instance.name = validated_data.get("name", instance.name)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get(
            "visibility",
            instance.visibility
        )
        instance.date = validated_data.get("date", instance.date)
        instance.save()

        # Handle ExerciseInstances

        self.update_exercise_instances(
            exercise_instances,
            exercise_instances_data
        )
        self.add_new_objects(
            instance,
            exercise_instances,
            exercise_instances_data,
            self.add_exercise_instance
        )
        self.delete_objects(exercise_instances, exercise_instances_data)

        # Handle WorkoutFiles

        if "files" in validated_data:
            files_data = validated_data.pop("files")
            files = instance.files

            for file, file_data in zip(files.all(), files_data):
                file.file = file_data.get("file", file.file)

            self.add_new_objects(
                instance,
                files,
                files_data,
                self.add_workout_file
            )
            self.delete_objects(files, files_data)

        return instance

    def get_owner_username(self, obj):
        """Returns the owning user's username

        Args:
            obj (Workout): Current Workout

        Returns:
            str: Username of owner
        """
        return obj.owner.username
