from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework import filters, generics, mixins, permissions
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.reverse import reverse

from secfit.mixins import CreateListModelMixin
from workouts.models import Workout, WorkoutFile
from workouts.parsers import MultipartJsonParser
from workouts.permissions import (
    IsOwner,
    IsCoachAndVisibleToCoach,
    IsOwnerOfWorkout,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsReadOnly,
    IsPublic,
    IsWorkoutPublic,
)
from workouts.serializers import (
    WorkoutSerializer,
    WorkoutStatisticSerializer,
    WorkoutFileSerializer
)
from workouts.utils import workout_filter, workout_object_filter


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "workouts": reverse(
                "workout-list", request=request, format=format
            ),
            "workout-files": reverse(
                "workout-file-list", request=request, format=format
            ),
        }
    )


class WorkoutList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of a Workout,
    or displaying a list of Workouts

    HTTP methods: GET, POST
    """

    serializer_class = WorkoutSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]
    parser_classes = [
        MultipartJsonParser,
        JSONParser,
    ]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["name", "date", "owner__username"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        query_set = Workout.objects.none()
        if self.request.user:
            query_set = Workout.objects.filter(
                workout_filter(self.request.user)
            ).distinct()

        return query_set


class WorkoutStatistic(mixins.ListModelMixin, generics.GenericAPIView):
    """Class defining the web response for the workout statistics

    HTTP methods: GET
    """

    serializer_class = WorkoutStatisticSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        query_set = Workout.objects.none()
        if self.request.user:
            status = self.request.query_params.get("user", None)

            # A workout should be visible to the requesting user if owner of
            # the workout is the requesting user
            if status and (
                status == "current" or status == str(self.request.user.id)
            ):
                query_set = Workout.objects.filter(
                    owner=self.request.user
                ).order_by(
                    "-date"
                )

            # A workout should be visible to the requesting user if the
            # workout is public or the workout has coach visibility and
            # the requesting user is the owner's coach
            elif status:
                try:
                    pk = int(status)
                    owner = get_user_model().objects.get(id=pk)
                    query_set = Workout.objects.filter(
                        Q(owner=owner)
                        & (
                            workout_filter(self.request.user)
                        )
                    ).order_by("-date")

                except Exception:
                    pass
        return query_set


class WorkoutDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the details of an individual Workout.

    HTTP methods: GET, PUT, DELETE
    """

    queryset = Workout.objects.all()
    serializer_class = WorkoutSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (IsOwner | (IsReadOnly & (IsCoachAndVisibleToCoach | IsPublic)))
    ]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class WorkoutFileList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):

    queryset = WorkoutFile.objects.all()
    serializer_class = WorkoutFileSerializer
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfWorkout]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        query_set = WorkoutFile.objects.none()
        if self.request.user:
            query_set = WorkoutFile.objects.filter(
                workout_object_filter(self.request.user)
            ).distinct()

        return query_set


class WorkoutFileDetail(
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    queryset = WorkoutFile.objects.all()
    serializer_class = WorkoutFileSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (
            IsOwner
            | IsOwnerOfWorkout
            | (
                IsReadOnly
                & (IsCoachOfWorkoutAndVisibleToCoach | IsWorkoutPublic)
            )
        )
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
