from django.db.models import Q
from .models import Workout


def workout_filter(user):
    return Q(
        Q(visibility=Workout.PUBLIC)
        | Q(owner=user)
        | (Q(visibility=Workout.COACH) & Q(owner__coach=user))
    )


def workout_object_filter(user):
    return Q(
        Q(workout__visibility=Workout.PUBLIC)
        | Q(workout__owner=user)
        | (
            Q(workout__visibility=Workout.COACH)
            & Q(workout__owner__coach=user)
        )
    )
