from django.contrib.auth import get_user_model
from django.db import models


class Workout(models.Model):
    """Django model for a workout that users can log.

    A workout has several attributes, and is associated with one or more
    exercises (instances) and, optionally, files uploaded by the user.

    Attributes:
        name:        Name of the workout
        date:        Date the workout was performed or is planned
        notes:       Notes about the workout
        owner:       User that logged the workout
        visibility:  The visibility level of the workout:
                     Public, Coach, or Private
    """

    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    notes = models.TextField()
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="workouts"
    )

    # Visibility levels
    PUBLIC = "PUBLIC"
    COACH = "COACH"
    PRIVATE = "PRIVATE"
    VISIBILITY_CHOICES = [
        (PUBLIC, "Public"),
        (COACH, "Coach"),
        (PRIVATE, "Private"),
    ]

    visibility = models.CharField(
        max_length=7, choices=VISIBILITY_CHOICES, default=COACH
    )

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return self.name


def workout_directory_path(instance, filename):
    """Return path for which workout files should be uploaded on the web server

    Args:
        instance (WorkoutFile): WorkoutFile instance
        filename (str): Name of the file

    Returns:
        str: Path where workout file is stored
    """
    return f"workouts/{instance.workout.id}/{filename}"


class WorkoutFile(models.Model):
    """Django model for file associated with a workout. Basically a wrapper.

    Attributes:
        workout: The workout for which this file has been uploaded
        owner:   The user who uploaded the file
        file:    The actual file that's being uploaded
    """

    workout = models.ForeignKey(
        Workout,
        on_delete=models.CASCADE,
        related_name="files"
    )
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="workout_files"
    )
    file = models.FileField(upload_to=workout_directory_path)
