from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from workouts import views

urlpatterns = format_suffix_patterns(
    [
        path("", views.api_root),
        path(
            "workouts/",
            views.WorkoutList.as_view(),
            name="workout-list"
        ),
        path(
            "workout-statistics/",
            views.WorkoutStatistic.as_view(),
            name="workout-statistic",
        ),
        path(
            "workouts/<int:pk>/",
            views.WorkoutDetail.as_view(),
            name="workout-detail",
        ),
        path(
            "workout-files/",
            views.WorkoutFileList.as_view(),
            name="workout-file-list",
        ),
        path(
            "workout-files/<int:pk>/",
            views.WorkoutFileDetail.as_view(),
            name="workoutfile-detail",
        ),
    ]
)
