from django.db import models

from workouts.models import Workout


class Exercise(models.Model):
    """Django model for an exercise type that users can create.

    Each exercise instance must have an exercise type,
    e.g., Pushups, Crunches, or Lunges.

    Attributes:
        name:        Name of the exercise type
        description: Description of the exercise type
        unit:        Name of the unit for the exercise type
                     (e.g., reps, seconds)
    """

    name = models.CharField(max_length=100)
    description = models.TextField()
    unit = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class ExerciseInstance(models.Model):
    """Django model for an instance of an exercise.

    Each workout has one or more exercise instances, each of a given type.
    For example, Kyle's workout on 15.06.2029 had one exercise instance:
    3 (sets) reps (unit) of 10 (repetitions) pushups (exercise type)

    Attributes:
        workout:    The workout associated with this exercise instance
        exercise:   The exercise type of this instance
        sets:       The number of sets the owner will perform/performed
        repetitions:     The number of repetitions in each set the owner will
                    perform/performed
    """

    workout = models.ForeignKey(
        Workout, on_delete=models.CASCADE, related_name="exercise_instances"
    )
    exercise = models.ForeignKey(
        Exercise, on_delete=models.CASCADE, related_name="instances"
    )
    sets = models.IntegerField()
    repetitions = models.IntegerField()
