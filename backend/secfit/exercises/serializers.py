from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField

from exercises.models import Exercise, ExerciseInstance
from workouts.models import Workout


class ExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an ExerciseInstance. Hyperlinks are used for
    relationships by default.

    Attributes:
        workout:    The associated workout for this instance, represented by
                    a hyperlink
    """

    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(),
        view_name="workout-detail", required=False
    )

    class Meta:
        model = ExerciseInstance
        fields = ["url", "id", "exercise", "sets", "repetitions", "workout"]


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Exercise.
    Hyperlinks are used for relationships by default.

    Attributes:
        instances:  Associated exercise instances with this Exercise type.
                    Hyperlinks.
    """

    instances = serializers.HyperlinkedRelatedField(
        many=True, view_name="exerciseinstance-detail", read_only=True
    )

    class Meta:
        model = Exercise
        fields = ["url", "id", "name", "description", "unit", "instances"]
