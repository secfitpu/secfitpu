from django.urls import path

from exercises import views

urlpatterns = [
    path(
        "exercises/",
        views.ExerciseList.as_view(),
        name="exercise-list"),
    path(
        "exercises/<int:pk>/",
        views.ExerciseDetail.as_view(),
        name="exercise-detail",
    ),
    path(
        "exercise-instances/",
        views.ExerciseInstanceList.as_view(),
        name="exercise-instance-list",
    ),
    path(
        "exercise-instances/<int:pk>/",
        views.ExerciseInstanceDetail.as_view(),
        name="exerciseinstance-detail",
    ),
]
