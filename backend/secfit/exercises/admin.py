from django.contrib import admin

from .models import Exercise, ExerciseInstance

admin.site.register(Exercise)
admin.site.register(ExerciseInstance)
