from django.urls import path

from comments.views import CommentDetail, CommentList, LikeDetail, LikeList

urlpatterns = [
    path("comments/", CommentList.as_view(), name="comment-list"),
    path("comments/<int:pk>/",
         CommentDetail.as_view(),
         name="comment-detail"),
    path("likes/", LikeList.as_view(), name="like-list"),
    path("likes/<int:pk>/", LikeDetail.as_view(), name="like-detail"),
]
