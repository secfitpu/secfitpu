from django.contrib.auth import get_user_model
from django.db import models

from workouts.models import Workout


class Comment(models.Model):
    """Django model for a comment left on a workout.

    Attributes:
        owner:       Who posted the comment
        workout:     The workout this comment was left on.
        content:     The content of the comment.
        timestamp:   When the comment was created.
    """
    related_name = "comments"

    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name=related_name
    )
    workout = models.ForeignKey(
        Workout, on_delete=models.CASCADE, related_name=related_name
    )
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-timestamp"]


class Like(models.Model):
    """Django model for a reaction to a comment.


    Attributes:
        owner:       Who liked the comment
        comment:     The comment that was liked
        timestamp:   When the like occurred.
    """
    related_name = "likes"

    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name=related_name
    )
    comment = models.ForeignKey(
        Comment,
        on_delete=models.CASCADE,
        related_name=related_name)
    timestamp = models.DateTimeField(auto_now_add=True)
